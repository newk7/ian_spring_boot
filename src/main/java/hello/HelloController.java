package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.Random;

@RestController
public class HelloController {
    
    @RequestMapping("/hello")
    public String index() {
        return "Greetings from Spring Boot!";
	}

	@RequestMapping("/get_time")
	public String time() {
		long millis=System.currentTimeMillis();
		java.util.Date date=new java.util.Date(millis);
		return date.toString();
    }
	
	@RequestMapping("/get_random")
	public int rando() {
		Random rand = new Random();
		int rando_num=rand.nextInt(1000);
		return rando_num;
	}
    
}
